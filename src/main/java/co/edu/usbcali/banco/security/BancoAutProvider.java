package co.edu.usbcali.banco.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.view.ServiceDelegate;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;


/**
* @author Zathura Code Generator http://code.google.com/p/zathura/
* www.zathuracode.org
*
*/
@Scope("singleton")
@Component("bancoAutProvider")
public class BancoAutProvider implements AuthenticationProvider {
    /**
     * Security Implementation
     */
	@Autowired
	private ServiceDelegate serviceDelegate;
	
    @Override
    public Authentication authenticate(Authentication authentication)
        throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        /// Condicion corregida
        
        	try {
				if(serviceDelegate.loginUsuario(name, password)) {
					Usuario usuario = serviceDelegate.findUsuarioById(name);
				    final List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
				    grantedAuths.add(new SimpleGrantedAuthority("ROLE_"+usuario.getTipoUsuario().getNombre().replaceAll(" ", "_").toUpperCase()));
				    final UserDetails principal = new User(name, password, grantedAuths);
				    final Authentication auth = new UsernamePasswordAuthenticationToken(principal,
				            password, grantedAuths);
				    ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).setAttribute("USUARIO_SESION",usuario);
				    return auth;
				}
			} catch (Exception e) {
				return null;
			
			}
		return null;
        
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
