package co.edu.usbcali.banco.view;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.dto.CuentaRegistradaDTO;
import co.edu.usbcali.banco.dto.TransaccionDTO;
import co.edu.usbcali.banco.services.ClienteService;
import co.edu.usbcali.banco.services.CuentaRegistradaService;
import co.edu.usbcali.banco.services.CuentaService;
import co.edu.usbcali.banco.services.EnviarCorreoService;
import co.edu.usbcali.banco.services.TipoDocumentoService;
import co.edu.usbcali.banco.services.TipoTransaccionService;
import co.edu.usbcali.banco.services.TipoUsuarioService;
import co.edu.usbcali.banco.services.TransaccionBancariaService;
import co.edu.usbcali.banco.services.TransaccionService;
import co.edu.usbcali.banco.services.UsuarioService;

@Component("serviceDelegate")
@Scope("singleton")
public class ServiceDelegateImpl implements ServiceDelegate {
	
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	@Autowired
	private CuentaService cuentaService;
	@Autowired
	private CuentaRegistradaService cuentaRegistradaService;
	@Autowired
	private TipoTransaccionService tipoTransaccionService;
	@Autowired
	private TipoUsuarioService tipoUsuarioService;
	@Autowired
	private TransaccionService transaccionService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private TransaccionBancariaService transaccionBancariaService;
	@Autowired
	private EnviarCorreoService enviarCorreoService;
	@Override
	public void saveCliente(Cliente cliente) throws Exception {
		clienteService.save(cliente);

	}

	@Override
	public void updateCliente(Cliente cliente) throws Exception {
		clienteService.update(cliente);

	}

	@Override
	public void deleteCliente(Cliente cliente) throws Exception {
		clienteService.delete(cliente);

	}

	@Override
	public Cliente findClienteById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return clienteService.findById(id);
	}

	@Override
	public List<Cliente> findClienteAll() throws Exception {
		// TODO Auto-generated method stub
		return clienteService.findAll();
	}

	@Override
	public void saveTipoDocumento(TipoDocumento tipoDocumento) throws Exception {
		tipoDocumentoService.save(tipoDocumento);

	}

	@Override
	public void updateTipoDocumento(TipoDocumento tipoDocumento) throws Exception {
		tipoDocumentoService.update(tipoDocumento);

	}

	@Override
	public void deleteTipoDocumento(TipoDocumento tipoDocumento) throws Exception {
		tipoDocumentoService.delete(tipoDocumento);

	}

	@Override
	public TipoDocumento findTipoDocumentoById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return tipoDocumentoService.findById(id);
	}

	@Override
	public List<TipoDocumento> findTipoDocumentoAll() throws Exception {
		// TODO Auto-generated method stub
		return tipoDocumentoService.findAll();
	}

	@Override
	public void saveCuenta(Cuenta cuenta) throws Exception {
		// TODO Auto-generated method stub
		cuentaService.save(cuenta);
	}

	@Override
	public void updateCuenta(Cuenta cuenta) throws Exception {
		// TODO Auto-generated method stub
		cuentaService.update(cuenta);
	}

	@Override
	public void deleteCuenta(Cuenta cuenta) throws Exception {
		// TODO Auto-generated method stub
		cuentaService.delete(cuenta);
	}

	@Override
	public Cuenta findCuentaById(String id) throws Exception {
		// TODO Auto-generated method stub
		return cuentaService.findById(id);
	}

	@Override
	public List<Cuenta> findAllCuenta() throws Exception {
		// TODO Auto-generated method stub
		return cuentaService.findAll();
	}

	@Override
	public void saveCuentaRegistrada(CuentaRegistrada cuentaRegistrada) throws Exception {
		// TODO Auto-generated method stub
		cuentaRegistradaService.save(cuentaRegistrada);
	
	}

	@Override
	public void updateCuentaRegistrada(CuentaRegistrada cuentaRegistrada) throws Exception {
		// TODO Auto-generated method stub
		cuentaRegistradaService.update(cuentaRegistrada);
	}

	@Override
	public void deleteCuentaRegistrada(CuentaRegistrada cuentaRegistrada) throws Exception {
		// TODO Auto-generated method stub
		cuentaRegistradaService.delete(cuentaRegistrada);
	}

	@Override
	public CuentaRegistrada findCuentaRegistradaById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return cuentaRegistradaService.findById(id);
	}

	@Override
	public List<CuentaRegistrada> findAllCuentaRegistrada() throws Exception {
		// TODO Auto-generated method stub
		return cuentaRegistradaService.findAll();
	}

	@Override
	public void saveTipoTransaccion(TipoTransaccion tipoTransaccion) throws Exception {
		// TODO Auto-generated method stub
		tipoTransaccionService.save(tipoTransaccion);
	}

	@Override
	public void updateTipoTransaccion(TipoTransaccion tipoTransaccion) throws Exception {
		// TODO Auto-generated method stub
		tipoTransaccionService.update(tipoTransaccion);
	}

	@Override
	public void deleteTipoTransaccion(TipoTransaccion tipoTransaccion) throws Exception {
		// TODO Auto-generated method stub
		tipoTransaccionService.delete(tipoTransaccion);
	}

	@Override
	public TipoTransaccion findTipoTransaccionById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return tipoTransaccionService.findById(id);
	}

	@Override
	public List<TipoTransaccion> findAllTipoTransaccion() throws Exception {
		// TODO Auto-generated method stub
		return tipoTransaccionService.findAll();
	}

	@Override
	public void saveTipoUsuario(TipoUsuario tipoUsuario) throws Exception {
		// TODO Auto-generated method stub
		tipoUsuarioService.save(tipoUsuario);
	}

	@Override
	public void updateTipoUsuario(TipoUsuario tipoUsuario) throws Exception {
		// TODO Auto-generated method stub
		tipoUsuarioService.update(tipoUsuario);
		
	}

	@Override
	public void deleteTipoUsuario(TipoUsuario tipoUsuario) throws Exception {
		// TODO Auto-generated method stub
		tipoUsuarioService.delete(tipoUsuario);
	}

	@Override
	public TipoUsuario findTipoUsuarioById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return tipoUsuarioService.findById(id);
	}

	@Override
	public List<TipoUsuario> findAllTipoUsuario() throws Exception {
		// TODO Auto-generated method stub
		return tipoUsuarioService.findAll();
	}

	@Override
	public Transaccion saveTransaccion(Transaccion transaccion) throws Exception {
		// TODO Auto-generated method stub
		 return transaccionService.save(transaccion);
	}

	@Override
	public void updateTransaccion(Transaccion transaccion) throws Exception {
		// TODO Auto-generated method stub
		transaccionService.update(transaccion);
		
	}

	@Override
	public void deleteTransaccion(Transaccion transaccion) throws Exception {
		// TODO Auto-generated method stub
		transaccionService.delete(transaccion);
	}

	@Override
	public Transaccion findTransaccionById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return transaccionService.findById(id);
	}

	@Override
	public List<Transaccion> findAllTransaccion() throws Exception {
		// TODO Auto-generated method stub
		return transaccionService.findAll();
	}

	@Override
	public void saveUsuario(Usuario usuario) throws Exception {
		// TODO Auto-generated method stub
		usuarioService.save(usuario);
		
	}

	@Override
	public void updateUsuario(Usuario usuario) throws Exception {
		// TODO Auto-generated method stub
		usuarioService.update(usuario);
	}

	@Override
	public void deleteUsuario(Usuario usuario) throws Exception {
		// TODO Auto-generated method stub
		usuarioService.delete(usuario);
	}

	@Override
	public Usuario findUsuarioById(String id){
		// TODO Auto-generated method stub
		return usuarioService.findById(id);
	}

	@Override
	public List<Usuario> findAllUsuario() throws Exception {
		// TODO Auto-generated method stub
		return usuarioService.findAll();
	}

	@Override
	public List<CuentaRegistradaDTO> findAllCuentaRegistradaDTO() throws Exception {
		// TODO Auto-generated method stub
		return cuentaRegistradaService.findAllDTO();
	}

	@Override
	public List<TransaccionDTO> findAllTransaccionDTO() throws Exception {
		// TODO Auto-generated method stub
		return transaccionService.findAllDTO();
	}

	@Override
	public void enviarCorreoCreacionCuenta(String correo, String cuenId, String clave) throws Exception {
		// TODO Auto-generated method stub
		enviarCorreoService.enviarCorreoCreacionCuenta(correo, cuenId, clave);
		
	}

	@Override
	public void enviarCorreoTransaccion(String correo, BigDecimal valor, String cuen_id,
			TipoTransaccion tipoTransaccion) throws Exception {
		enviarCorreoService.enviarCorreoTransaccion(correo, valor, cuen_id, tipoTransaccion);
		
	}

	@Override
	public Boolean loginUsuario(String usuUsuario, String clave) throws Exception {
		// TODO Auto-generated method stub
		return usuarioService.loginUsuario(usuUsuario, clave);
	}

	@Override
	public void inactivar(Cuenta cuenta) throws Exception {
		// TODO Auto-generated method stub
		 cuentaService.inactivar(cuenta);
	}

	@Override
	public void inactivarUsuario(String usuUsuario) throws Exception {
		// TODO Auto-generated method stub
		usuarioService.inactivarUsuario(usuUsuario);
	}

	@Override
	public Cuenta consignar(String cuenId, String usuUsuario, BigDecimal valor) throws Exception {
		// TODO Auto-generated method stub
		return transaccionBancariaService.consignar(cuenId, usuUsuario, valor);
	}

	@Override
	public Cuenta retirar(String cuenId, String usuUsuario, BigDecimal valor) throws Exception {
		// TODO Auto-generated method stub
		return transaccionBancariaService.retirar(cuenId, usuUsuario, valor);
	}

}
