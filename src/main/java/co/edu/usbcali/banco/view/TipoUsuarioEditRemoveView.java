package co.edu.usbcali.banco.view;


import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.TipoUsuario;

@ManagedBean
@ViewScoped
public class TipoUsuarioEditRemoveView {
	
	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value="#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private List<TipoUsuario> listaTipoUsuario;
	private InputText txtIdTipoUsuario;
	private InputText txtNombreTipoUsuario;
	private SelectOneMenu somActivo;
	private CommandButton btnCrear;
	private CommandButton btnModificar; 
	private CommandButton btnBorrar;
	private Boolean dialogTipoUsuarioVisible = false;
	public void crearActionListener(ActionEvent actionEvent) {
		try {
			limpiarAction();
			dialogTipoUsuarioVisible=true;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
	
		}
	}
	
	public void modificarActionListener(ActionEvent actionEvent) {
		TipoUsuario tipoUsuario = (TipoUsuario)actionEvent.getComponent().getAttributes().get("tipoUsuarioModificar");
		try {
			if(tipoUsuario!=null) {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombreTipoUsuario.setValue(tipoUsuario.getNombre());
				txtIdTipoUsuario.setValue(tipoUsuario.getTiusId());
				somActivo.setValue(tipoUsuario.getActivo());
				dialogTipoUsuarioVisible = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		
		}
	}
	
	public void borrarActionListener(ActionEvent actionEvent) {
		TipoUsuario tipoUsuario = (TipoUsuario)actionEvent.getComponent().getAttributes().get("tipoUsuarioBorrar");
		try {
			if(tipoUsuario!=null) {
				setDialogTipoUsuarioVisible(false);
				serviceDelegate.deleteTipoUsuario(tipoUsuario);
				listaTipoUsuario=null;
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El tipo de usuario se borró con éxito", ""));

			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		
	}
	
	private void limpiar() {
		btnCrear.setDisabled(false);
		btnModificar.setDisabled(true);
		btnBorrar.setDisabled(true);
		txtNombreTipoUsuario.resetValue();
		somActivo.resetValue();
	}
	public String limpiarAction() {
		limpiar();
		txtIdTipoUsuario.resetValue();
		btnCrear.setDisabled(true);
		return "";
	}
	public String crearAction() {
		try {
			TipoUsuario tiUs = new TipoUsuario();
			tiUs.setActivo(somActivo.getValue().toString());
			tiUs.setNombre(txtNombreTipoUsuario.getValue().toString().toUpperCase());
			tiUs.setTiusId(Long.parseLong(txtIdTipoUsuario.getValue().toString()));
			serviceDelegate.saveTipoUsuario(tiUs);
			listaTipoUsuario= null;
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,  "El Tipo de usuario se creó con éxito",""));
		} catch (Exception e) {
			 FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));
		}
		return "";
	}
	
	public String modificarAction() {
		try {
			TipoUsuario tiUs = serviceDelegate.findTipoUsuarioById(Long.parseLong(txtIdTipoUsuario.getValue().toString()));
			tiUs.setActivo(somActivo.getValue().toString());
			tiUs.setNombre(txtNombreTipoUsuario.getValue().toString().toUpperCase());
			serviceDelegate.updateTipoUsuario(tiUs);
			listaTipoUsuario=null;
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,  "El Tipo de usuario se modificó con éxito",""));
		} catch (Exception e) {
			 FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));
		}
		return "";
	}
	public String borrarAction() {
		try {
			TipoUsuario tiUs = serviceDelegate.findTipoUsuarioById(Long.parseLong(txtIdTipoUsuario.getValue().toString()));
			serviceDelegate.deleteTipoUsuario(tiUs);
			limpiarAction();
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,  "El Tipo de usuario se eliminó con éxito",""));
			
		} catch (Exception e) {
			// TODO: handle exception
			 FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));
				
		}
		return "";
	}
	
	public void txtIdTipoUsuListener() {
		try {
			Long id = Long.parseLong(txtIdTipoUsuario.getValue().toString());
			TipoUsuario tipoUsuario = serviceDelegate.findTipoUsuarioById(id);
			if(tipoUsuario==null) {
				limpiar();
			}else {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombreTipoUsuario.setValue(tipoUsuario.getNombre());
				somActivo.setValue(tipoUsuario.getActivo());
			}
		} catch (Exception e) {
			limpiar();
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El id no es válido", ""));
			
		}
	}
	
	public InputText getTxtIdTipoUsuario() {
		return txtIdTipoUsuario;
	}
	public void setTxtIdTipoUsuario(InputText txtIdTipoUsuario) {
		this.txtIdTipoUsuario = txtIdTipoUsuario;
	}
	public InputText getTxtNombreTipoUsuario() {
		return txtNombreTipoUsuario;
	}
	public void setTxtNombreTipoUsuario(InputText txtNombreTipoUsuario) {
		this.txtNombreTipoUsuario = txtNombreTipoUsuario;
	}
	public SelectOneMenu getSomActivo() {
		return somActivo;
	}
	public void setSomActivo(SelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}
	public CommandButton getBtnCrear() {
		return btnCrear;
	}
	public void setBtnCrear(CommandButton btnCrear) {
		this.btnCrear = btnCrear;
	}
	public CommandButton getBtnModificar() {
		return btnModificar;
	}
	public void setBtnModificar(CommandButton btnModificar) {
		this.btnModificar = btnModificar;
	}
	public CommandButton getBtnBorrar() {
		return btnBorrar;
	}
	public void setBtnBorrar(CommandButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}
	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}
	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}
	public List<TipoUsuario> getListaTipoUsuario() {
		if (listaTipoUsuario==null) {
			try {
				listaTipoUsuario= serviceDelegate.findAllTipoUsuario();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return listaTipoUsuario;
	}
	public void setListaTipoUsuario(List<TipoUsuario> listaTipoUsuario) {
		this.listaTipoUsuario = listaTipoUsuario;
	}

	public Boolean getDialogTipoUsuarioVisible() {
		return dialogTipoUsuarioVisible;
	}

	public void setDialogTipoUsuarioVisible(Boolean dialogTipoUsuarioVisible) {
		this.dialogTipoUsuarioVisible = dialogTipoUsuarioVisible;
	}
	
	

}
