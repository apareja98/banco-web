package co.edu.usbcali.banco.view;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.dto.CuentaRegistradaDTO;

@ManagedBean
@ViewScoped
public class CuentaRegistradaView {
	
	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value="#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private List<CuentaRegistradaDTO> listaCuentasRegistradas;
	
	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}
	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}
	public List<CuentaRegistradaDTO> getListaCuentasRegistradas() {
		if(listaCuentasRegistradas == null) {
			try {
				listaCuentasRegistradas = serviceDelegate.findAllCuentaRegistradaDTO();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return listaCuentasRegistradas;
	}
	public void setListaCuentasRegistradas(List<CuentaRegistradaDTO> listaCuentasRegistradas) {
		this.listaCuentasRegistradas = listaCuentasRegistradas;
	}

}
