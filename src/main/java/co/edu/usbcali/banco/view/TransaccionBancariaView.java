package co.edu.usbcali.banco.view;

import java.math.BigDecimal;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.core.tools.picocli.CommandLine.Command;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.password.Password;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.Usuario;

@ManagedBean
@ViewScoped
public class TransaccionBancariaView {

	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value = "#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private InputText txtIdCuenta;
	private BigDecimal valor = new BigDecimal(0);
	private String option;
	private CommandButton btnRealizar;

	private void limpiar() {
		txtIdCuenta.resetValue();
		valor = new BigDecimal(0);
	}

	public String limpiarAction() {
		limpiar();

		return "";
	}

	public String realizarTransaccionAction() {
		try {
			Usuario usuarioSesion = (Usuario) ((HttpSession) FacesContext.getCurrentInstance().getExternalContext()
					.getSession(true)).getAttribute("USUARIO_SESION");
			if(option.trim().equalsIgnoreCase("1")) {
				serviceDelegate.consignar(txtIdCuenta.getValue().toString(), usuarioSesion.getUsuUsuario(),
						valor);
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "La consignación se realizó con éxito", ""));

			}
			else if (option.trim().equalsIgnoreCase("2")) {
				serviceDelegate.retirar(txtIdCuenta.getValue().toString(), usuarioSesion.getUsuUsuario(),
						valor);
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El retiro se realizó con éxito", ""));
				
			}else{
				
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Debes seleccionar un tipo de transacción", ""));
			}
		
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		return "";
	}

	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}

	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public CommandButton getBtnRealizar() {
		return btnRealizar;
	}

	public void setBtnRealizar(CommandButton btnRealizar) {
		this.btnRealizar = btnRealizar;
	}

	public InputText getTxtIdCuenta() {
		return txtIdCuenta;
	}

	public void setTxtIdCuenta(InputText txtIdCuenta) {
		this.txtIdCuenta = txtIdCuenta;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	

}
