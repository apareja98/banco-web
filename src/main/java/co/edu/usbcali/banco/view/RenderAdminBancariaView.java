package co.edu.usbcali.banco.view;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.dto.TransaccionDTO;

@ManagedBean
@ViewScoped
public class RenderAdminBancariaView {
	
	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value="#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private Boolean renderOpcionesCliente;
	private Usuario usuarioEnSesion;
	private Boolean renderOpcionesAdmin;
	private Boolean renderOpcionesCajero;
	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}
	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}
	public Boolean getRenderOpcionesCliente() {
		Usuario usuario = getUsuarioEnSesion();
		if(usuario.getTipoUsuario().getTiusId()==2L || usuario.getTipoUsuario().getTiusId()==3L)return true;
		return false;
	}
	public void setrenderOpcionesCliente(Boolean renderAdmin) {
		this.setRenderOpcionesCliente(renderAdmin);
	}
	public Usuario getUsuarioEnSesion() {
		if(usuarioEnSesion==null) {
			this.usuarioEnSesion= (Usuario) ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute("USUARIO_SESION");
			
		}
		return this.usuarioEnSesion;
	}
	public void setUsuarioEnSesion(Usuario usuarioEnSesion) {
		this.usuarioEnSesion = usuarioEnSesion;
	}
	public void setRenderOpcionesCliente(Boolean renderOpcionesCliente) {
		this.renderOpcionesCliente = renderOpcionesCliente;
	}
	public Boolean getRenderOpcionesAdmin() {
		Usuario usuario = getUsuarioEnSesion();
		if(usuario.getTipoUsuario().getTiusId()==3L)return true;
		return false;
	}
	public void setRenderOpcionesAdmin(Boolean renderOpcionesAdmin) {
		this.renderOpcionesAdmin = renderOpcionesAdmin;
	}
	public Boolean getRenderOpcionesCajero() {
		Usuario usuario = getUsuarioEnSesion();
		if(usuario.getTipoUsuario().getTiusId()==1L)return true;
		return false;
	}
	public void setRenderOpcionesCajero(Boolean renderOpcionesCajero) {
		this.renderOpcionesCajero = renderOpcionesCajero;
	}
	
		
	

}
