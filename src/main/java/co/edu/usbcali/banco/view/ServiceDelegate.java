package co.edu.usbcali.banco.view;

import java.math.BigDecimal;
import java.util.List;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.dto.CuentaRegistradaDTO;
import co.edu.usbcali.banco.dto.TransaccionDTO;

public interface ServiceDelegate {

	public void saveCliente(Cliente cliente) throws Exception;

	public void updateCliente(Cliente cliente) throws Exception;

	public void deleteCliente(Cliente cliente) throws Exception;

	public Cliente findClienteById(Long id) throws Exception;

	public List<Cliente> findClienteAll() throws Exception;

	public void saveTipoDocumento(TipoDocumento tipoDocumento) throws Exception;

	public void updateTipoDocumento(TipoDocumento tipoDocumento) throws Exception;

	public void deleteTipoDocumento(TipoDocumento tipoDocumento) throws Exception;

	public TipoDocumento findTipoDocumentoById(Long id) throws Exception;

	public List<TipoDocumento> findTipoDocumentoAll() throws Exception;

	public void saveCuenta(Cuenta cuenta) throws Exception;

	public void updateCuenta(Cuenta cuenta) throws Exception;

	public void deleteCuenta(Cuenta cuenta) throws Exception;

	public Cuenta findCuentaById(String id) throws Exception;

	public List<Cuenta> findAllCuenta() throws Exception;

	public void saveCuentaRegistrada(CuentaRegistrada cuentaRegistrada) throws Exception;

	public void updateCuentaRegistrada(CuentaRegistrada cuentaRegistrada) throws Exception;

	public void deleteCuentaRegistrada(CuentaRegistrada cuentaRegistrada) throws Exception;

	public CuentaRegistrada findCuentaRegistradaById(Long id) throws Exception;

	public List<CuentaRegistrada> findAllCuentaRegistrada() throws Exception;

	public void saveTipoTransaccion(TipoTransaccion tipoTransaccion) throws Exception;

	public void updateTipoTransaccion(TipoTransaccion tipoTransaccion) throws Exception;

	public void deleteTipoTransaccion(TipoTransaccion tipoTransaccion) throws Exception;

	public TipoTransaccion findTipoTransaccionById(Long id) throws Exception;

	public List<TipoTransaccion> findAllTipoTransaccion() throws Exception;
	
	public void saveTipoUsuario(TipoUsuario tipoUsuario) throws Exception;

	public void updateTipoUsuario(TipoUsuario tipoUsuario) throws Exception;

	public void deleteTipoUsuario(TipoUsuario tipoUsuario) throws Exception;

	public TipoUsuario findTipoUsuarioById(Long id) throws Exception;

	public List<TipoUsuario> findAllTipoUsuario() throws Exception;

	public Transaccion saveTransaccion(Transaccion transaccion) throws Exception;

	public void updateTransaccion(Transaccion transaccion) throws Exception;

	public void deleteTransaccion(Transaccion transaccion) throws Exception;

	public Transaccion findTransaccionById(Long id) throws Exception;

	public List<Transaccion> findAllTransaccion() throws Exception;
	
	public void saveUsuario(Usuario usuario) throws Exception;

	public void updateUsuario(Usuario usuario) throws Exception;

	public void deleteUsuario(Usuario usuario) throws Exception;

	public Usuario findUsuarioById(String id) ;

	public List<Usuario> findAllUsuario() throws Exception;
	
	public List<CuentaRegistradaDTO> findAllCuentaRegistradaDTO() throws Exception;
	
	public List<TransaccionDTO> findAllTransaccionDTO() throws Exception; 
	
	public void enviarCorreoCreacionCuenta(String correo,  String cuenId, String clave)throws Exception;
	
	public void enviarCorreoTransaccion(String correo, BigDecimal valor, String cuen_id, TipoTransaccion tipoTransaccion) throws Exception;
	
	public Boolean loginUsuario(String usuUsuario, String clave) throws Exception;
	
	public void inactivar(Cuenta cuenta)throws Exception;
	
	public void inactivarUsuario(String usuUsuario)throws Exception;
	
	public Cuenta consignar(String cuenId, String usuUsuario, BigDecimal valor) throws Exception;
	
	public Cuenta retirar(String cuenId, String usuUsuario, BigDecimal valor) throws Exception;
	
}
