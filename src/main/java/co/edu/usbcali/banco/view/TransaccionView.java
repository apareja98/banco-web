package co.edu.usbcali.banco.view;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.dto.TransaccionDTO;

@ManagedBean
@ViewScoped
public class TransaccionView {
	
	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value="#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private List<TransaccionDTO> listaTransaccion;
	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}
	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}
	public List<TransaccionDTO> getListaTransaccion() {
		if(listaTransaccion==null) {
			try {
				listaTransaccion=serviceDelegate.findAllTransaccionDTO();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return listaTransaccion;
	}
	
	public void setListaTransaccion(List<TransaccionDTO> listaTransaccion) {
		this.listaTransaccion = listaTransaccion;
	}
	
	

}
