package co.edu.usbcali.banco.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.password.Password;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.domain.Usuario;

@ManagedBean
@ViewScoped
public class UsuarioEditRemoveView {

	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value = "#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private List<Usuario> listaUsuario;
	private InputText txtNombreUsuario;
	private InputText txtNombre;
	private InputText txtIdentificacion;
	private Password txtClave;
	private SelectOneMenu somActivo;
	private SelectOneMenu somTipoUsuario;
	private List<SelectItem> losTiposUsuarioSelectItem;
	private CommandButton btnCrear;
	private CommandButton btnModificar;
	private CommandButton btnLimpiar;
	private CommandButton btnBorrar;
	private Boolean dialogUsuarioVisible = false;

	private void limpiar() {
		btnCrear.setDisabled(false);
		btnModificar.setDisabled(true);
		btnBorrar.setDisabled(true);
		txtNombre.resetValue();
		txtIdentificacion.resetValue();
		txtClave.resetValue();
		somActivo.setValue("-1");
		somTipoUsuario.setValue("-1");
	}

	public String limpiarAction() {
		limpiar();
		txtNombreUsuario.resetValue();
		btnCrear.setDisabled(true);
		return "";
	}

	public void modificarActionListener(ActionEvent actionEvent) {
		Usuario usuario= (Usuario)actionEvent.getComponent().getAttributes().get("usuarioModificar");
		try {
			if(usuario!=null) {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombreUsuario.setValue(usuario.getUsuUsuario());
				txtNombre.setValue(usuario.getNombre());
				txtIdentificacion.setValue(usuario.getIdentificacion());
				txtClave.setValue(usuario.getClave());
				somActivo.setValue(usuario.getActivo());
				somTipoUsuario.setValue(usuario.getTipoUsuario().getTiusId());
				dialogUsuarioVisible=true;
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
	}

	public void crearActionListener(ActionEvent actionEvent) {
		log.info("crear");
		try {
			limpiarAction();
			dialogUsuarioVisible = true;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
	}

	public void borrarActionListener(ActionEvent actionEvent) {
		Usuario usuario= (Usuario)actionEvent.getComponent().getAttributes().get("usuarioBorrar");
		try {
			if(usuario!=null) {
				setDialogUsuarioVisible(false);
				serviceDelegate.deleteUsuario(usuario);
				listaUsuario = null;
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El usuario se borró con éxito", ""));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
	
	}

	public String modificarAction() {
		try {
			Usuario usuario = serviceDelegate.findUsuarioById(txtNombreUsuario.getValue().toString());

			usuario.setActivo(somActivo.getValue().toString());
			usuario.setNombre(txtNombre.getValue().toString());
			usuario.setClave(txtClave.getValue().toString());
			usuario.setIdentificacion(new BigDecimal(txtIdentificacion.getValue().toString()));
			TipoUsuario tipoUsu = serviceDelegate
					.findTipoUsuarioById(Long.parseLong(somTipoUsuario.getValue().toString()));
			usuario.setTipoUsuario(tipoUsu);
			serviceDelegate.updateUsuario(usuario);
			listaUsuario=null;
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El Usuario se modificó con éxito", ""));

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		return "";

	}

	public String borrarAction() {
		try {
			Usuario usuario = serviceDelegate.findUsuarioById(txtNombreUsuario.getValue().toString());
			serviceDelegate.deleteUsuario(usuario);
			limpiar();
			
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El Usuario se eliminó con éxito", ""));

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		return "";
	}

	public String crearAction() {
		try {
			Usuario usuario = new Usuario();
			usuario.setActivo(somActivo.getValue().toString());
			usuario.setUsuUsuario(txtNombreUsuario.getValue().toString());
			usuario.setNombre(txtNombre.getValue().toString());
			usuario.setClave(txtClave.getValue().toString());
			usuario.setIdentificacion(new BigDecimal(txtIdentificacion.getValue().toString()));
			TipoUsuario tipoUsu = serviceDelegate
					.findTipoUsuarioById(Long.parseLong(somTipoUsuario.getValue().toString()));
			usuario.setTipoUsuario(tipoUsu);
			serviceDelegate.saveUsuario(usuario);
			listaUsuario=null;
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El Usuario se creó con éxito", ""));

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		return "";
	}

	public void txtNombreUsuarioListener() {
		try {
			String nombreUsuario = txtNombreUsuario.getValue().toString();
			Usuario usuario = serviceDelegate.findUsuarioById(nombreUsuario);
			if (usuario == null) {
				limpiar();
			} else {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombre.setValue(usuario.getNombre());
				txtIdentificacion.setValue(usuario.getIdentificacion());
				txtClave.setValue(usuario.getClave());
				somActivo.setValue(usuario.getActivo());
				somTipoUsuario.setValue(usuario.getTipoUsuario().getTiusId());
			}
		} catch (Exception e) {
			limpiar();
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "El nombre de usuario no es válido", ""));

		}
	}

	public InputText getTxtNombreUsuario() {
		return txtNombreUsuario;
	}

	public void setTxtNombreUsuario(InputText txtNombreUsuario) {
		this.txtNombreUsuario = txtNombreUsuario;
	}

	public InputText getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(InputText txtNombre) {
		this.txtNombre = txtNombre;
	}

	public InputText getTxtIdentificacion() {
		return txtIdentificacion;
	}

	public void setTxtIdentificacion(InputText txtIdentificacion) {
		this.txtIdentificacion = txtIdentificacion;
	}

	public Password getTxtClave() {
		return txtClave;
	}

	public void setTxtClave(Password txtClave) {
		this.txtClave = txtClave;
	}

	public SelectOneMenu getSomActivo() {
		return somActivo;
	}

	public void setSomActivo(SelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}

	public SelectOneMenu getSomTipoUsuario() {
		return somTipoUsuario;
	}

	public void setSomTipoUsuario(SelectOneMenu somTipoUsuario) {
		this.somTipoUsuario = somTipoUsuario;
	}

	public List<SelectItem> getLosTiposUsuarioSelectItem() {
		try {
			if (losTiposUsuarioSelectItem == null) {
				losTiposUsuarioSelectItem = new ArrayList<>();
				List<TipoUsuario> losTipoUsuario = serviceDelegate.findAllTipoUsuario();
				losTipoUsuario.forEach(tipoUsu -> {
					losTiposUsuarioSelectItem.add(new SelectItem(tipoUsu.getTiusId(), tipoUsu.getNombre()));
				});
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
		}
		return losTiposUsuarioSelectItem;
	}

	public void setLosTiposUsuarioSelectItem(List<SelectItem> losTiposUsuarioSelectItem) {
		this.losTiposUsuarioSelectItem = losTiposUsuarioSelectItem;
	}

	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}

	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}

	public List<Usuario> getListaUsuario() {
		if (listaUsuario == null) {
			try {
				listaUsuario = serviceDelegate.findAllUsuario();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return listaUsuario;
	}

	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public CommandButton getBtnCrear() {
		return btnCrear;
	}

	public void setBtnCrear(CommandButton btnCrear) {
		this.btnCrear = btnCrear;
	}

	public CommandButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(CommandButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(CommandButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}

	public Boolean getDialogUsuarioVisible() {
		return dialogUsuarioVisible;
	}

	public void setDialogUsuarioVisible(Boolean dialogUsuarioVisible) {
		this.dialogUsuarioVisible = dialogUsuarioVisible;
	}

}
