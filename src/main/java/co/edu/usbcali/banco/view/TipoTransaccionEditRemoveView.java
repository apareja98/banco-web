package co.edu.usbcali.banco.view;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.domain.TipoTransaccion;
import co.edu.usbcali.banco.domain.TipoUsuario;

@ManagedBean
@ViewScoped
public class TipoTransaccionEditRemoveView {
	
	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value="#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private List<TipoTransaccion> listaTipoTransaccion;
	
	private InputText txtIdTipoTran;
	private InputText txtNombreTipoTran;
	private SelectOneMenu somActivo;
	private CommandButton btnCrear;
	private CommandButton btnModificar; 
	private CommandButton btnBorrar;
	private Boolean dialogTipoTransaccionVisible = false;
	
	public void crearActionListener(ActionEvent actionEvent) {
		try {
			limpiarAction();
			dialogTipoTransaccionVisible=true;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
	
		}
	}
	
	public void modificarActionListener(ActionEvent actionEvent) {
		TipoTransaccion tipoTransaccion = (TipoTransaccion)actionEvent.getComponent().getAttributes().get("tipoTransaccionModificar");
		try {
			if(tipoTransaccion!=null) {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombreTipoTran.setValue(tipoTransaccion.getNombre());
				txtIdTipoTran.setValue(tipoTransaccion.getTitrId());
				somActivo.setValue(tipoTransaccion.getActivo());
				dialogTipoTransaccionVisible = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		
		}
	}
	
	public void borrarActionListener(ActionEvent actionEvent) {
		TipoTransaccion tipoTransaccion = (TipoTransaccion)actionEvent.getComponent().getAttributes().get("tipoTransaccionBorrar");
		try {
			if(tipoTransaccion!=null) {
				setDialogTipoTransaccionVisible(false);
				serviceDelegate.deleteTipoTransaccion(tipoTransaccion);
				listaTipoTransaccion=null;
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El tipo de transacción se borró con éxito", ""));

			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		
	}
	
	private void limpiar() {
		btnCrear.setDisabled(false);
		btnModificar.setDisabled(true);
		btnBorrar.setDisabled(true);
		txtNombreTipoTran.resetValue();
		somActivo.resetValue();
	}
	public String limpiarAction() {
		limpiar();
		txtIdTipoTran.resetValue();
		btnCrear.setDisabled(true);
		return "";
	}
	
	public String crearAction() {
		try {
			TipoTransaccion tiTran = new TipoTransaccion();
			tiTran.setActivo(somActivo.getValue().toString());
			tiTran.setNombre(txtNombreTipoTran.getValue().toString().toUpperCase());
			tiTran.setTitrId(Long.parseLong(txtIdTipoTran.getValue().toString()));
			serviceDelegate.saveTipoTransaccion(tiTran);
			listaTipoTransaccion=null;
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,  "El Tipo de transaccion se creó con éxito",""));
		} catch (Exception e) {
			 FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));
		}
		return "";
	}
	
	
	public String modificarAction() {
		try {
			TipoTransaccion tiTran = serviceDelegate.findTipoTransaccionById(Long.parseLong(txtIdTipoTran.getValue().toString()));
			tiTran.setActivo(somActivo.getValue().toString());
			tiTran.setNombre(txtNombreTipoTran.getValue().toString().toUpperCase());
			serviceDelegate.updateTipoTransaccion(tiTran);
			listaTipoTransaccion=null;
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,  "El Tipo de transaccion se modificó con éxito",""));
		} catch (Exception e) {
			 FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));
		}
		return "";
		
	}
	public String borrarAction() {
		try {
			TipoTransaccion tiTran = serviceDelegate.findTipoTransaccionById(Long.parseLong(txtIdTipoTran.getValue().toString()));
			serviceDelegate.deleteTipoTransaccion(tiTran);
			limpiar();
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,  "El Tipo de transaccion se eliminó con éxito",""));
			
		}catch (Exception e) {
			 FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));
				
		}
		return "";
	}
	public void txtIdTipoTranListener() {
		try {
			Long id = Long.parseLong(txtIdTipoTran.getValue().toString());
			TipoTransaccion tipoTransaccion = serviceDelegate.findTipoTransaccionById(id);
			if(tipoTransaccion==null) {
				limpiar();
			}else {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombreTipoTran.setValue(tipoTransaccion.getNombre());
				somActivo.setValue(tipoTransaccion.getActivo());
			}
		} catch (Exception e) {
			limpiar();
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El id no es válido", ""));
			
		}
	}
	public InputText getTxtIdTipoTran() {
		return txtIdTipoTran;
	}
	public void setTxtIdTipoTran(InputText txtIdTipoTran) {
		this.txtIdTipoTran = txtIdTipoTran;
	}
	public InputText getTxtNombreTipoTran() {
		return txtNombreTipoTran;
	}
	public void setTxtNombreTipoTran(InputText txtNombreTipoTran) {
		this.txtNombreTipoTran = txtNombreTipoTran;
	}
	public SelectOneMenu getSomActivo() {
		return somActivo;
	}
	public void setSomActivo(SelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}
	public CommandButton getBtnCrear() {
		return btnCrear;
	}
	public void setBtnCrear(CommandButton btnCrear) {
		this.btnCrear = btnCrear;
	}
	public CommandButton getBtnModificar() {
		return btnModificar;
	}
	public void setBtnModificar(CommandButton btnModificar) {
		this.btnModificar = btnModificar;
	}
	public CommandButton getBtnBorrar() {
		return btnBorrar;
	}
	public void setBtnBorrar(CommandButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}
	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}
	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}
	public List<TipoTransaccion> getListaTipoTransaccion() {
		if(listaTipoTransaccion==null) {
			try {
				listaTipoTransaccion = serviceDelegate.findAllTipoTransaccion();
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e.getMessage());
			}
		}
		return listaTipoTransaccion;
	}
	
	public void setListaTipoTransaccion(List<TipoTransaccion> listaTipoTransaccion) {
		this.listaTipoTransaccion = listaTipoTransaccion;
	}
	public Boolean getDialogTipoTransaccionVisible() {
		return dialogTipoTransaccionVisible;
	}
	public void setDialogTipoTransaccionVisible(Boolean dialogTipoTransaccionVisible) {
		this.dialogTipoTransaccionVisible = dialogTipoTransaccionVisible;
	}
	

}
