
package co.edu.usbcali.banco.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.hibernate.sql.Select;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.TipoDocumento;;

@ManagedBean
@ViewScoped
public class ClienteEditRemoveView {
	private final static Logger log = LoggerFactory.getLogger(ClienteEditRemoveView.class);
	@ManagedProperty(value = "#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private InputText txtIdentificacion;
	private InputText txtNombre;
	private InputText txtDireccion;
	private InputText txtTelefono;
	private InputText txtEmail;
	private Boolean dialogClienteVisible = false;
	private SelectOneMenu somActivo;
	private SelectOneMenu somTipoDocumento;

	private CommandButton btnCrear;
	private CommandButton btnModificar;
	private CommandButton btnBorrar;

	public void crearActionListener(ActionEvent actionEvent) {
		try {
			btnCrear.setDisabled(false);
			btnModificar.setDisabled(true);
			btnBorrar.setDisabled(true);
			txtEmail.resetValue();
			txtDireccion.resetValue();
			txtIdentificacion.setDisabled(false);
			txtIdentificacion.resetValue();
			txtNombre.resetValue();
			txtTelefono.resetValue();
			somActivo.setValue("-1");
			somTipoDocumento.setValue("-1");
			dialogClienteVisible = true;

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		
		}

	}

	public void modificarActionListener(ActionEvent actionEvent) {
		Cliente cliente = (Cliente) actionEvent.getComponent().getAttributes().get("clienteModificar");
		try {

			if (cliente != null) {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtIdentificacion.setValue(cliente.getClieId());
				txtDireccion.setValue(cliente.getDireccion());
				txtNombre.setValue(cliente.getNombre());
				txtTelefono.setValue(cliente.getTelefono());
				txtEmail.setValue(cliente.getEmail());
				somActivo.setValue(cliente.getActivo());
				somTipoDocumento.setValue(cliente.getTipoDocumento().getTdocId().toString());

				txtIdentificacion.setDisabled(true);
				dialogClienteVisible = true;

			}

		} catch (Exception e) {
			// TODO: handle exception

			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}

	}

	public void borrarActionListener(ActionEvent actionEvent) {
		Cliente cliente = (Cliente) actionEvent.getComponent().getAttributes().get("clienteBorrar");
		try {
			if (cliente != null) {
				dialogClienteVisible = false;
				serviceDelegate.deleteCliente(cliente);
				losClientes = null;
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El cliente se borró con exito", ""));
			}

		} catch (Exception e) {
			// TODO: handle exception

			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
	}

	private void limpiar() {
		btnCrear.setDisabled(false);
		btnModificar.setDisabled(true);
		btnBorrar.setDisabled(true);
		txtEmail.resetValue();
		txtDireccion.resetValue();
		txtNombre.resetValue();
		txtTelefono.resetValue();
		somActivo.setValue("-1");
		somTipoDocumento.setValue("-1");
	}

	public String limpiarAction() {
		limpiar();
		txtIdentificacion.resetValue();
		btnCrear.setDisabled(true);
		txtIdentificacion.setDisabled(false);
		return "";
	}

	public void txtIdentificacionListener() {
		try {
			Long id = Long.parseLong(txtIdentificacion.getValue().toString());
			Cliente cliente = serviceDelegate.findClienteById(id);
			if (cliente == null) {
				limpiar();
			} else {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtDireccion.setValue(cliente.getDireccion());
				txtNombre.setValue(cliente.getNombre());
				txtTelefono.setValue(cliente.getTelefono());
				txtEmail.setValue(cliente.getEmail());
				somActivo.setValue(cliente.getActivo());
				somTipoDocumento.setValue(cliente.getTipoDocumento().getTdocId().toString());

			}
		} catch (Exception e) {
			limpiar();

			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "La identificación no es válida", ""));
		}
	}

	public String crearAction() {

		try {
			Cliente cliente = new Cliente();
			cliente.setActivo(somActivo.getValue().toString());
			cliente.setClieId(Long.parseLong(txtIdentificacion.getValue().toString()));
			cliente.setDireccion(txtDireccion.getValue().toString());
			cliente.setEmail(txtEmail.getValue().toString());
			cliente.setNombre(txtNombre.getValue().toString());
			cliente.setTelefono(txtTelefono.getValue().toString());
			TipoDocumento tipoDocumento = serviceDelegate
					.findTipoDocumentoById(Long.parseLong(somTipoDocumento.getValue().toString()));
			cliente.setTipoDocumento(tipoDocumento);
			serviceDelegate.saveCliente(cliente);
			Cuenta cuenta = new Cuenta();
			cuenta.setActiva("S");
			cuenta.setClave(null);
			cuenta.setCliente(cliente);
			cuenta.setCuenId(null);
			cuenta.setSaldo(new BigDecimal("0"));
			serviceDelegate.saveCuenta(cuenta);
			losClientes = null;
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El cliente se creó con éxito", ""));
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Se creó una cuenta al cliente con id de cuenta: "+ cuenta.getCuenId()  , ""));


		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

		}
		return "";
	}

	public String modificarAction() {

		try {
			Cliente cliente = serviceDelegate.findClienteById(Long.parseLong(txtIdentificacion.getValue().toString()));

			cliente.setActivo(somActivo.getValue().toString());
			cliente.setClieId(Long.parseLong(txtIdentificacion.getValue().toString()));
			cliente.setDireccion(txtDireccion.getValue().toString());
			cliente.setEmail(txtEmail.getValue().toString());
			cliente.setNombre(txtNombre.getValue().toString());
			cliente.setTelefono(txtTelefono.getValue().toString());
			TipoDocumento tipoDocumento = serviceDelegate
					.findTipoDocumentoById(Long.parseLong(somTipoDocumento.getValue().toString()));
			cliente.setTipoDocumento(tipoDocumento);
			serviceDelegate.updateCliente(cliente);
			losClientes = null;
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El cliente se modificó con éxito", ""));

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

		}
		return "";

	}

	public String borrarAction() {
		try {
			Cliente cliente = serviceDelegate.findClienteById(Long.parseLong(txtIdentificacion.getValue().toString()));
			serviceDelegate.deleteCliente(cliente);
			limpiar();
			
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El cliente se eliminó con éxito", ""));

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

		}
		return "";

	}

	private List<Cliente> losClientes;

	private List<SelectItem> losTiposDocumentosSelectItem;

	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}

	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}

	public List<Cliente> getLosClientes() {
		if (losClientes == null) {
			try {
				losClientes = serviceDelegate.findClienteAll();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return losClientes;
	}

	public void setLosClientes(List<Cliente> losClientes) {
		this.losClientes = losClientes;
	}

	public InputText getTxtIdentificacion() {
		return txtIdentificacion;
	}

	public void setTxtIdentificacion(InputText txtIdentificacion) {
		this.txtIdentificacion = txtIdentificacion;
	}

	public InputText getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(InputText txtNombre) {
		this.txtNombre = txtNombre;
	}

	public InputText getTxtDireccion() {
		return txtDireccion;
	}

	public void setTxtDireccion(InputText txtDireccion) {
		this.txtDireccion = txtDireccion;
	}

	public InputText getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(InputText txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public InputText getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(InputText txtEmail) {
		this.txtEmail = txtEmail;
	}

	public SelectOneMenu getSomActivo() {
		return somActivo;
	}

	public void setSomActivo(SelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}

	public SelectOneMenu getSomTipoDocumento() {
		return somTipoDocumento;
	}

	public void setSomTipoDocumento(SelectOneMenu somTipoDocumento) {
		this.somTipoDocumento = somTipoDocumento;
	}

	public CommandButton getBtnCrear() {
		return btnCrear;
	}

	public void setBtnCrear(CommandButton btnCrear) {
		this.btnCrear = btnCrear;
	}

	public CommandButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(CommandButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public CommandButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(CommandButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}

	public List<SelectItem> getLosTiposDocumentosSelectItem() {

		try {
			if (losTiposDocumentosSelectItem == null) {
				losTiposDocumentosSelectItem = new ArrayList<>();
				List<TipoDocumento> losItem = serviceDelegate.findTipoDocumentoAll();
				for (TipoDocumento tipoDocumento : losItem) {
					losTiposDocumentosSelectItem
							.add(new SelectItem(tipoDocumento.getTdocId(), tipoDocumento.getNombre()));

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
		}
		return losTiposDocumentosSelectItem;
	}

	public void setLosTiposDocumentosSelectItem(List<SelectItem> losTiposDocumentosSelectItem) {
		this.losTiposDocumentosSelectItem = losTiposDocumentosSelectItem;
	}

	public Boolean getDialogClienteVisible() {
		return dialogClienteVisible;
	}

	public void setDialogClienteVisible(Boolean dialogClienteVisible) {
		this.dialogClienteVisible = dialogClienteVisible;
	}

}
