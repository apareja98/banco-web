package co.edu.usbcali.banco.view;


import java.math.BigDecimal;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.password.Password;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;

@ManagedBean
@ViewScoped
public class CuentaEditRemoveView {

	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value = "#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private List<Cuenta> listaCuentas;
	private InputMask txtIdCuenta;
	private Password txtClave;
	private InputText txtSaldo;
	private InputText txtIdCliente;
	private SelectOneMenu somActivo;
	private OutputLabel lblNombreCliente;
	private CommandButton btnCrear;
	private CommandButton btnModificar;
	private CommandButton btnBorrar;
	private Cliente cliente;
	private Boolean dialogCuentaVisible = false;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	
	public void crearActionListener(ActionEvent actionEvent) {
		try {
			limpiarAction();
			dialogCuentaVisible= true;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		
		}
	}
	public void modificarActionListener(ActionEvent actionEvent) {
		Cuenta cuenta = (Cuenta) actionEvent.getComponent().getAttributes().get("cuentaModificar");
		try {
			if (cuenta != null) {
				
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtIdCuenta.setValue(cuenta.getCuenId());
				txtClave.setDisabled(false);
				txtClave.setValue(cuenta.getClave());
				txtSaldo.setValue(cuenta.getSaldo());
				txtIdCliente.setValue(cuenta.getCliente().getClieId());
				somActivo.setValue(cuenta.getActiva());
				cliente = cuenta.getCliente();
				lblNombreCliente.setValue(cliente.getNombre());
				dialogCuentaVisible = true;
				
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
	}
	public void borrarActionListener(ActionEvent actionEvent) {
		Cuenta cuenta = (Cuenta) actionEvent.getComponent().getAttributes().get("cuentaBorrar");
		try {
			if (cuenta != null) {
				setDialogCuentaVisible(false);
				serviceDelegate.inactivar(cuenta);
				listaCuentas = null;
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "La cuenta se inactivó con éxito", ""));

			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
	}

	private void limpiar() {
		txtClave.resetValue();
		txtSaldo.setValue("0");
		somActivo.setValue("-1");
		txtIdCliente.resetValue();
		lblNombreCliente.setValue("-----");
		btnCrear.setDisabled(false);
		btnModificar.setDisabled(true);
		btnBorrar.setDisabled(true);
		txtClave.setDisabled(true);
		cliente = null;
	}
	public String limpiarAction() {
		limpiar();
		txtIdCuenta.resetValue();
		btnCrear.setDisabled(false);
		return "";
	}
	public void txtIdClienteListener() {
		try {
			
			Long clienteId = Long.parseLong(txtIdCliente.getValue().toString());
		    cliente = serviceDelegate.findClienteById(clienteId);
			if(cliente==null) {
				lblNombreCliente.setValue("-----");
			}else {
				lblNombreCliente.setValue(cliente.getNombre());
			}
		}catch(Exception e) {
			lblNombreCliente.setValue("-----");
		}
	}
	public String crearAction() {
		try {
			if(txtSaldo.getValue().toString()==null ||txtSaldo.getValue().toString().equals("") )txtSaldo.setValue("0");
			Cuenta cuenta = new Cuenta();
			cuenta.setActiva(somActivo.getValue().toString());
			cuenta.setClave(null);
			cuenta.setCliente(cliente);
			cuenta.setCuenId(null);
			cuenta.setSaldo(new BigDecimal(txtSaldo.getValue().toString()));
			serviceDelegate.saveCuenta(cuenta);
			txtIdCuenta.setValue(cuenta.getCuenId());
			txtClave.setValue(cuenta.getClave());
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "La cuenta se creó con éxito \n id es: " + cuenta.getCuenId() + "\n La clave es:" + cuenta.getClave() , ""));
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Se envió un correo con la información al usuario", ""));
			limpiarAction();
			listaCuentas = null;
			
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		return "";
	}
	public String modificarAction() {
		try {
			Cuenta cuenta = serviceDelegate.findCuentaById(txtIdCuenta.getValue().toString());
			cuenta.setActiva(somActivo.getValue().toString());
			cuenta.setClave(txtClave.getValue().toString());
			cuenta.setCliente(cliente);
			cuenta.setSaldo(new BigDecimal(txtSaldo.getValue().toString()));
			serviceDelegate.updateCuenta(cuenta);
			listaCuentas = null;
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "La cuenta se modificó con éxito", ""));
		}catch (Exception e) {
			// TODO: handle exception
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

		}
		return "";
	}
	
	public String borrarAction() {
		try {
			Cuenta cuenta = serviceDelegate.findCuentaById(txtIdCuenta.getValue().toString());
			serviceDelegate.inactivar(cuenta);
			limpiarAction();
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "La cuenta se inactivó con éxito", ""));
		
		} catch (Exception e) {
		
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

		}
		return "";
	}
	
	public void txtIdListener() {
		try {
			if(txtIdCuenta.getValue()==null || txtIdCuenta.getValue().equals("")){
				btnCrear.setDisabled(false);
			}
			String cuentaId = txtIdCuenta.getValue().toString().trim();
			//log.info(cuentaId);
			Cuenta cuenta = serviceDelegate.findCuentaById(cuentaId);
			
			if(cuenta==null) {
				limpiarAction();
			}else {
				
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtClave.setDisabled(false);
				txtClave.setValue(cuenta.getClave());
				txtSaldo.setValue(cuenta.getSaldo());
				txtIdCliente.setValue(cuenta.getCliente().getClieId());
				somActivo.setValue(cuenta.getActiva());
				cliente = cuenta.getCliente();
				lblNombreCliente.setValue(cliente.getNombre());
			}
		} catch (Exception e) {
			limpiar();
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cuenta no es válida", ""));
		}
	}

	public InputMask getTxtIdCuenta() {
		return txtIdCuenta;
	}

	public void setTxtIdCuenta(InputMask txtIdCuenta) {
		this.txtIdCuenta = txtIdCuenta;
	}

	public InputText getTxtSaldo() {
		return txtSaldo;
	}

	public void setTxtSaldo(InputText txtSaldo) {
		this.txtSaldo = txtSaldo;
	}

	public InputText getTxtIdCliente() {
		return txtIdCliente;
	}

	public void setTxtIdCliente(InputText txtIdCliente) {
		this.txtIdCliente = txtIdCliente;
	}

	public SelectOneMenu getSomActivo() {
		return somActivo;
	}

	public void setSomActivo(SelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}

	public OutputLabel getLblNombreCliente() {
		return lblNombreCliente;
	}

	public void setLblNombreCliente(OutputLabel lblNombreCliente) {
		this.lblNombreCliente = lblNombreCliente;
	}

	public CommandButton getBtnCrear() {
		return btnCrear;
	}

	public void setBtnCrear(CommandButton btnCrear) {
		this.btnCrear = btnCrear;
	}

	public CommandButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(CommandButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public CommandButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(CommandButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}

	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}

	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}

	public List<Cuenta> getListaCuentas() {
		if (listaCuentas == null) {
			try {
				listaCuentas = serviceDelegate.findAllCuenta();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return listaCuentas;
	}

	public void setListaCuentas(List<Cuenta> listaCuentas) {
		this.listaCuentas = listaCuentas;
	}

	public Password getTxtClave() {
		return txtClave;
	}

	public void setTxtClave(Password txtClave) {
		this.txtClave = txtClave;
	}

	public Boolean getDialogCuentaVisible() {
		return dialogCuentaVisible;
	}

	public void setDialogCuentaVisible(Boolean dialogCuentaVisible) {
		this.dialogCuentaVisible = dialogCuentaVisible;
	}

}
