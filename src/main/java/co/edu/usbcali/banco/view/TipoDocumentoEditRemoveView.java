package co.edu.usbcali.banco.view;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.banco.domain.TipoDocumento;


@ManagedBean
@ViewScoped
public class TipoDocumentoEditRemoveView {
	
	private final static Logger log = LoggerFactory.getLogger(ClienteView.class);
	@ManagedProperty(value="#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
	private List<TipoDocumento> listaTipoDocumento;
	private InputText txtIdTipoDoc;
	private InputText txtNombreTipoDoc;
	private SelectOneMenu somActivo;
	private CommandButton btnCrear;
	private CommandButton btnModificar; 
	private CommandButton btnBorrar;
	private Boolean dialogTipoDocumentoVisible =false;
	
	public void crearActionListener(ActionEvent actionEvent) {
		try {
			limpiarAction();
			dialogTipoDocumentoVisible=true;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
	
		}
	}
	
	public void modificarActionListener(ActionEvent actionEvent) {
		TipoDocumento tipoDocumento = (TipoDocumento)actionEvent.getComponent().getAttributes().get("tipoDocumentoModificar");
		try {
			if(tipoDocumento!=null) {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombreTipoDoc.setValue(tipoDocumento.getNombre());
				txtIdTipoDoc.setValue(tipoDocumento.getTdocId());
				somActivo.setValue(tipoDocumento.getActivo());
				dialogTipoDocumentoVisible = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		
		}
	}
	
	public void borrarActionListener(ActionEvent actionEvent) {
		TipoDocumento tipoDocumento = (TipoDocumento)actionEvent.getComponent().getAttributes().get("tipoDocumentoBorrar");
		
		try {
			if(tipoDocumento!=null) {
				setDialogTipoDocumentoVisible(false);
				serviceDelegate.deleteTipoDocumento(tipoDocumento);
				listaTipoDocumento=null;
				FacesContext.getCurrentInstance().addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El tipo de documento se borró con éxito", ""));

			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		
	}
	private void limpiar() {
		btnCrear.setDisabled(false);
		btnModificar.setDisabled(true);
		btnBorrar.setDisabled(true);
		txtNombreTipoDoc.resetValue();
		somActivo.resetValue();
	}
	public String limpiarAction() {
		limpiar();
		txtIdTipoDoc.resetValue();
		btnCrear.setDisabled(true);
		return "";
	}
	public String crearAction() {
		try {
			TipoDocumento tiDoc = new TipoDocumento();
			tiDoc.setActivo(somActivo.getValue().toString());
			tiDoc.setNombre(txtNombreTipoDoc.getValue().toString().toUpperCase());
			tiDoc.setTdocId(Long.parseLong(txtIdTipoDoc.getValue().toString()));
			serviceDelegate.saveTipoDocumento(tiDoc);
			listaTipoDocumento=null;
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO,  "El Tipo de documento se creó con éxito",""));
		} catch (Exception e) {
			 FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));
		}
		return "";
	}
	
	public String modificarAction() {
		try {
			TipoDocumento tiDoc = serviceDelegate.findTipoDocumentoById(Long.parseLong(txtIdTipoDoc.getValue().toString()));
			tiDoc.setActivo(somActivo.getValue().toString());
			tiDoc.setNombre(txtNombreTipoDoc.getValue().toString().toUpperCase());
			serviceDelegate.updateTipoDocumento(tiDoc);
			listaTipoDocumento=null;
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El tipo de documento se modificó con éxito", ""));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
		}
		return "";
	}
	public String borrarAction() {
		try {
			TipoDocumento tiDoc = serviceDelegate.findTipoDocumentoById(Long.parseLong(txtIdTipoDoc.getValue().toString()));
			serviceDelegate.deleteTipoDocumento(tiDoc);
			limpiar();
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El tipo de documento se eliminó con éxito", ""));
		
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

		}
		return "";
	}
	
	public InputText getTxtIdTipoDoc() {
		return txtIdTipoDoc;
	}
	public void setTxtIdTipoDoc(InputText txtIdTipoDoc) {
		this.txtIdTipoDoc = txtIdTipoDoc;
	}
	public InputText getTxtNombreTipoDoc() {
		return txtNombreTipoDoc;
	}
	public void setTxtNombreTipoDoc(InputText txtNombreTipoDoc) {
		this.txtNombreTipoDoc = txtNombreTipoDoc;
	}
	public SelectOneMenu getSomActivo() {
		return somActivo;
	}
	public void setSomActivo(SelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}
	public CommandButton getBtnCrear() {
		return btnCrear;
	}
	public void setBtnCrear(CommandButton btnCrear) {
		this.btnCrear = btnCrear;
	}
	public CommandButton getBtnModificar() {
		return btnModificar;
	}
	public void setBtnModificar(CommandButton btnModificar) {
		this.btnModificar = btnModificar;
	}
	public CommandButton getBtnBorrar() {
		return btnBorrar;
	}
	public void setBtnBorrar(CommandButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}
	public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}
	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}
	public List<TipoDocumento> getListaTipoDocumento() {
		if(listaTipoDocumento==null) {
			try {
				listaTipoDocumento = serviceDelegate.findTipoDocumentoAll();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return listaTipoDocumento;
	}
	public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}
	
	public void txtIdTipoDocListener() {
		try {
			Long id = Long.parseLong(txtIdTipoDoc.getValue().toString());
			TipoDocumento tipoDocumento = serviceDelegate.findTipoDocumentoById(id);
			if(tipoDocumento==null) {
				limpiar();
			}else {
				btnCrear.setDisabled(true);
				btnModificar.setDisabled(false);
				btnBorrar.setDisabled(false);
				txtNombreTipoDoc.setValue(tipoDocumento.getNombre());
				somActivo.setValue(tipoDocumento.getActivo());
				
			}
		} catch (Exception e) {
			limpiar();
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El id no es válido", ""));
		}
	}
	public Boolean getDialogTipoDocumentoVisible() {
		return dialogTipoDocumentoVisible;
	}
	public void setDialogTipoDocumentoVisible(Boolean dialogTipoDocumentoVisible) {
		this.dialogTipoDocumentoVisible = dialogTipoDocumentoVisible;
	}
	
	

}
