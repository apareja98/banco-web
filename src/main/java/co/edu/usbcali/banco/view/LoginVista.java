package co.edu.usbcali.banco.view;


import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import java.util.TreeMap;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;


@ViewScoped
@ManagedBean(name = "loginVista")
public class LoginVista {
    private String userId;
    private String password;
    
	@ManagedProperty(value="#{serviceDelegate}")
	private ServiceDelegate serviceDelegate;
    
    @ManagedProperty(value = "#{authenticationManager}")
    private AuthenticationManager authenticationManager = null;

    public AuthenticationManager getAuthenticationManager() {
        return authenticationManager;
    }

    public void setAuthenticationManager(
        AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public ServiceDelegate getServiceDelegate() {
		return serviceDelegate;
	}

	public void setServiceDelegate(ServiceDelegate serviceDelegate) {
		this.serviceDelegate = serviceDelegate;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public void fallosAuth(String usuario) throws Exception {
    	if(usuario!=null && !(usuario.trim().equals(""))) {
    		TreeMap<String, Integer> contador = new TreeMap<>();
        	if(((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute("AUTH_ERROR")==null){
        		contador.put(usuario.trim(), 1);
        		
        	}else {
        		contador =(TreeMap<String, Integer>) ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute("AUTH_ERROR");
        		if(contador.get(usuario.trim())==null) {
        			contador.put(usuario.trim(), 1);
        			}
        		else {
        			Integer fallos = contador.get(usuario.trim())+1;
        			contador.put(usuario.trim(),fallos );
        			if(fallos==3) {
        				serviceDelegate.inactivarUsuario(usuario);
        				FacesContext.getCurrentInstance().addMessage("",new FacesMessage(FacesMessage.SEVERITY_WARN, "Demasiados intentos, se inactivó el usuario", ""));
        			}
        			
        		}
        	}
        	((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).setAttribute("AUTH_ERROR",contador);
    		
    	}
    	

    }
    
    public String login() {
        try {
        	
            Authentication request = new UsernamePasswordAuthenticationToken(this.getUserId(), this.getPassword());
            Authentication result = authenticationManager.authenticate(request);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(result);

            ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).setAttribute("AUTH_ERROR",null);
        } catch (AuthenticationException e) {
          FacesContext.getCurrentInstance().addMessage("",new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario o clave no validos", ""));
          try {
			fallosAuth(this.getUserId());
		} catch (Exception e1) {
			return "/login.xhtml";
		}  
          return "/login.xhtml";
        }

        return "/xhtml/principal.xhtml";
    }
}
